# Websocket test with Rails API and React-app

From https://medium.com/@dakota.lillie/using-action-cable-with-react-c37df065f296

Uses ActionCable and [react-actioncable-provider](https://www.npmjs.com/package/react-actioncable-provider)

## Run locally

(after setup)

```
rails s
```

```
yarn start
```

Check that it works by having the app up in two browsers at the same time (although you can also see it working after creation of Conversation or Message)

## Notes

- Doesn't seem to work 100% of the time 🤔
- TODO: Look into [ActionCable adapters](https://edgeguides.rubyonrails.org/action_cable_overview.html#subscription-adapter)
- TODO: Update code to remove deprecation warnings